/**
 * @Commercial class is a child class of Customer class used to inherite the property of Customer.
 */
class Commercial extends Customer{
	
	int consumerNo;
	String consumerName;
	int prevReading;
	int currReading;
	@Override
	public void input_data() throws Exception  {

        Scanner sc2 = new Scanner(System.in);
		
        try {
    		System.out.println("Enter Consumer Number: ");
    		consumerNo = sc2.nextInt();
    		if(consumerNo<0) {
    			throw new InputDataException("Invalid consumer number");
    		}
    		}
    		catch(InputDataException ide) {
    			System.out.print(ide);
    			System.exit(0);

    		}
    		catch(Exception e) {
    			System.out.print(e);
    			System.out.println(" : Invalid consumer number");
    			System.exit(0);
    		}
    		try {
    		System.out.println("Enter Consumer Name: ");
    		consumerName = sc2.next();
    		for(char c : consumerName.toCharArray()) {
    			if(!Character.isAlphabetic(c)) {
    				throw new InputDataException("invalid consumer name");
    			}
    		}
    		if((consumerName) == null) {
    			throw new InputDataException("invalid consumer name");
    		}
    		}catch(InputDataException ide) {
    			System.out.print(ide);
    			System.exit(0);


    		}catch(Exception e) {
    			System.out.print(e);
    			System.out.println(" : Invalid consumer number");
    			System.exit(0);
    		}
    		
    		
    		try {
    		System.out.println("Enter Current units: ");
    		currReading = sc2.nextInt(); 
    		if(currReading<prevReading) {
    			throw new InputDataException("Invalid Current reading");
    		}
    		}
    		catch(InputDataException ide) {
    			System.out.print(ide);
    			System.exit(0);

    		}
    		catch(Exception e) {
    			System.out.print(e);
    			System.out.println(" : Invalid consumer number");
    			System.exit(0);
    		}
    		int flag = 0;
    		for(Customer ec : dom_custs) {
    			if(ec.getConsumerNo() == consumerNo && ec.getConsumerName().equals(consumerName)) {
    				 if(ec.getEBConn() == 2) {
	    				ec.bill_history.add(new DomesticBill(ec.getPrevReading(), ec.getCurrReading(), calculate_bill()));
	    				ec.setCurrReading(currReading);
	    				prevReading = ec.getPrevReading();
	    				ec.setUnits(currReading - ec.getPrevReading());
	    				ec.setBill(calculate_bill());
	    				flag = 1;
	    				break;
    				 }
    				 

    			}

    		}
    		
    		
    		if(flag == 0) {
    			Commercial ec = new Commercial();
    			ec.setConsumerNo(consumerNo);
    			ec.setConsumerName(consumerName);
    			ec.setEBConn(2);
    			ec.setCurrReading(currReading);
    			ec.setUnits(currReading - getPrevReading());
    			ec.setBill(calculate_bill());
    			dom_custs.add(ec);
    			ec.bill_history.add(new DomesticBill( prevReading, currReading, calculate_bill()));
    		}
    	
	}

	@Override
	public double calculate_bill() throws Exception  {
		int units;
		double bill;
		units = currReading-prevReading;
		if(units >= 0 && units <= 100)
			bill = units * 2;
        else if( units>100 && units<= 200)
        	bill = (100*2) + ((units -100)*4.50);
        else if( units>200 && units<= 500)
        	bill = (100*2) + (100*4.50) + ((units - 200)*6);
        else 
        	bill = (100*2) + (100*4.50) + (300*6) + ((units-500)*7);

		if(units==0||bill<0) {
			throw new CalculationException("Invalid unit");
		}
		
		return bill;
		
		
	}

	@Override
	public void display()throws Exception {
	for(Customer c : dom_custs) {
		if(consumerNo == c.getConsumerNo()) {
			System.out.println("_________________________");
			System.out.println("ELECTRICITY BILL");
			System.out.println("__________________________");
			System.out.println("Consumer Number:" + c.getConsumerNo());
			System.out.println("Consumer Name:" + c.getConsumerName());
			System.out.println("Consumer Previous Reading:" + c.getPrevReading());
			System.out.println("Consumer Current Reading:" + c.getCurrReading());
			System.out.println("Type of Connection: Commercial" );
			System.out.println("________________________");
			System.out.println("Total Bill in Rs." + calculate_bill());
				
			}
		}
				
	}
	public String getCustomerDetails() {
		String result = "No record";
		for(Customer c : dom_custs) {
			
			if(consumerNo == c.getConsumerNo()) {
				result =  c.toString();
				System.out.println(c.getPrevReading());
				c.setPrevReading(c.getCurrReading());

			}
		}
		return result;
	}
		
}
