/**
 * @import import is basically used to import the different packages of java in our program.
 */

import java.util.ArrayList;
import java.io.*;
import java.util.Scanner;

/**
 * @Electricity_Bill_Gen This is our public class which contains main method.
 */

public class Electricity_Bill_Gen {
	 static int EBConn;
	 
/**
 * @param args is a reference variable of String Array. 
 * @throws Exception used to hand over the Exception object to JVM. 
 */
	  
	public static void main(String[] args)throws Exception {
		Scanner sc= new Scanner(System.in);

		System.out.println(" Welcome Electricity Bill Genetation ");
		System.out.println("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ \n");
	
		String option = "y"; 
		while(option.equals("y") || option.equals("yes")) {
			
		System.out.println("Connection Type: 1.Domestic 2.Commercial (1 or 2):");
	
			try {
				EBConn = sc.nextInt();
				if(!(EBConn == 1 || EBConn == 2)) {
					throw new InputDataException("Invalid connection type");
				}
			}catch(InputDataException ide) {
				System.out.println(ide);
			}catch(Exception e) {
				System.out.print(e);
				System.out.println(" : Invalid connection Type");
			}
			
/**
 * @switch is used to choose the type of EBConn(Domestic,Commercial).
 * A @FileWriter object is passed to the @BufferedWriter as the intent here is to write to some output file using a BufferedWriter.
 *  And finally, a @PrintWriter is used for print* methods like @println()			
 */
			
			switch(EBConn) {
			case 1:Domestic DM= new Domestic();
			DM.input_data();
			DM.display();
			DM.setEBConn(EBConn);

			BufferedWriter BW1 = new BufferedWriter(new FileWriter("Domestic.txt",true));
			PrintWriter pw1 = new PrintWriter(BW1);
			String custDetails = DM.getCustomerDetails();
			pw1.println(custDetails);
			
			pw1.close();
			System.out.println(custDetails);
			break;
			
			case 2:Commercial CM = new Commercial();
			CM.input_data() ;
			CM.display();
			CM.setEBConn(EBConn);
			BufferedWriter BW2 = new BufferedWriter(new FileWriter("Commercial.txt",true));
			PrintWriter pw2 = new PrintWriter(BW2);
			
			custDetails = CM.getCustomerDetails();
			pw2.println(custDetails);
			pw2.close();
			System.out.println(custDetails);
		
			}
			System.out.println("Enter 'y' or 'yes' to continue");
			option = sc.next().toLowerCase();
		}
			
	}
}

/**
 * @abstract class restrict the instantiation.
 * At least contains one method whose implementation is not done in the class.
 * That method  will be implemented in its child class.
 * @private is used to restrict the direct access of variable in other class
 * @ArrayList class uses a dynamic array for storing the elements.
 */

abstract class Customer{
	private int ConsumerNo;
	private String ConsumerName;
	private int  PrevReading = 0;
	private int CurrReading;
	private int EBConn;
	private int units;
	private double bill;
	
	ArrayList<Bill> bill_history = new ArrayList<>();

	static ArrayList<Customer> dom_comm = new ArrayList<>();

	abstract public void input_data()throws Exception;
	abstract public double calculate_bill()throws Exception;
	abstract public void display()throws Exception;
	
	public int getConsumerNo() {
		return ConsumerNo;
	}
	public void setConsumerNo(int consumerNo) {
		ConsumerNo = consumerNo;
	}
	public String getConsumerName() {
		return ConsumerName;
	}
	public void setConsumerName(String consumerName) {
		ConsumerName = consumerName;
	}
	public int getPrevReading() {
		return PrevReading;
	}
	public void setPrevReading(int prevReading) {
		PrevReading = prevReading;
	}
	public int getCurrReading() {
		return CurrReading;
	}
	public void setCurrReading(int currReading) {
		CurrReading = currReading;
	}
	public int getEBConn() {
		return EBConn;
	}
	public void setEBConn(int eBConn) {
		EBConn = eBConn;
	}
	public int getUnits() {
		return units;
	}
	public void setUnits(int units) {
		this.units = units;
	}
	public double getBill() {
		return bill;
	}
	public void setBill(double bill) {
		this.bill = bill;
	}
	@Override
	public String toString() {
		return "Customer [ConsumerNo=" + this.ConsumerNo + ", ConsumerName=" + this.ConsumerName + ", PrevReading=" + this.PrevReading
				+ ", CurrReading=" + this.CurrReading + ", EBConn=" + this.EBConn + ", units=" + this.units + ", bill=" + this.bill + "]";
	}
}

/**
 * @Domestic class is a child class of Customer class used to inherit the property of Customer.
 */

class Domestic extends Customer{

	int consumerNo;
	String consumerName;
	int prevReading ;
	int currReading;

	@Override
	public void input_data()throws Exception {
		
        Scanner sc1 = new Scanner(System.in);
		try {
		System.out.println("Enter Consumer Number: ");
		consumerNo = sc1.nextInt();
		if(consumerNo<0) {
			throw new InputDataException("Invalid consumer number");
		}
		}
		catch(InputDataException ide) {
			System.out.print(ide);
			System.exit(0);

		}
		catch(Exception e) {
			System.out.print(e);
			System.out.println(" : Invalid consumer number");
			System.exit(0);
		}
		
		try {
		System.out.println("Enter Consumer Name: ");
		consumerName = sc1.next();
		for(char c : consumerName.toCharArray()) {
			if(!Character.isAlphabetic(c)) {
				throw new InputDataException("invalid consumer name");
			}
		}
		if((consumerName) == null) {
			throw new InputDataException("invalid consumer name");
		}
		}catch(InputDataException ide) {
			System.out.print(ide);
			System.exit(0);


		}catch(Exception e) {
			System.out.print(e);
			System.out.println(" : Invalid consumer number");
			System.exit(0);
		}
		
		
		try {
		System.out.println("Enter Current units: ");
		currReading = sc1.nextInt(); 
		if(currReading<prevReading) {
			throw new InputDataException("Invalid Current reading");
		}
		}
		catch(InputDataException ide) {
			System.out.print(ide);
			System.exit(0);

		}
		catch(Exception e) {
			System.out.print(e);
			System.out.println(" : Invalid consumer number");
			System.exit(0);
		}
		
		
		int flag = 0;
		for(Customer ec : dom_comm) {
			if(ec.getConsumerNo() == consumerNo && ec.getConsumerName().equals(consumerName)) {
				ec.bill_history.add(new Bill(ec.getPrevReading(), ec.getCurrReading(), calculate_bill()));
				ec.setCurrReading(currReading);
				prevReading = ec.getPrevReading();
				ec.setUnits(currReading - ec.getPrevReading());
				ec.setBill(calculate_bill());
				flag = 1;
				break;

			}

		}
		if(flag == 0) {
			Domestic ec = new Domestic();
			ec.setConsumerNo(consumerNo);
			ec.setConsumerName(consumerName);
			ec.setEBConn(1);
			ec.setCurrReading(currReading);
			ec.setUnits(currReading - getPrevReading());
			ec.setBill(calculate_bill());
			dom_comm.add(ec);
			ec.bill_history.add(new Bill( prevReading, currReading, calculate_bill()));

		}
	}
	@Override
	public double calculate_bill() throws Exception {
		int units;
		double bill;
		units = currReading-prevReading;
		if(units >= 0 && units <= 100)
			bill = units * 1;
        else if( units>100 && units<= 200)
        	bill = (100*1) + ((units -100)*2.50);
        else if( units>200 && units<= 500)
        	bill = (100*1) + (100*2.50) + ((units - 200)*4);
        else 
        	bill = (100*1) + (100*2.50) + (300*4) + ((units-500)*6);
		
		if(units==0||bill<0) {
			throw new CalculationException("Invalid unit");
		}		
		return bill;
	}

	@Override
	public void display() throws Exception{
		for(Customer c : dom_comm) {
			if(consumerNo == c.getConsumerNo()) {
				System.out.println("_________________________");
				System.out.println("ELECTRICITY BILL");
				System.out.println("__________________________");
				System.out.println("Consumer Number:" + c.getConsumerNo());
				System.out.println("Consumer Name:" + c.getConsumerName());
				System.out.println("Consumer Previous Reading:" + c.getPrevReading());
				System.out.println("Consumer Current Reading:" + c.getCurrReading());
				System.out.println("Type of Connection: Domestic" );
				System.out.println("________________________");
				System.out.println("Total Bill in Rs." + calculate_bill());

			}
		}
				
	}
	public String getCustomerDetails() {
		String result = "No record";
		for(Customer c : dom_comm) {
			
			if(consumerNo == c.getConsumerNo()) {
				result =  c.toString();
				
				c.setPrevReading(c.getCurrReading());

			}
		}
		return result;
	}
			
}

/**
 * @Commercial class is a child class of Customer class used to inherit the property of Customer.
 */

class Commercial extends Customer{
	
	int consumerNo;
	String consumerName;
	int prevReading;
	int currReading;
	@Override
	public void input_data() throws Exception  {

        Scanner sc2 = new Scanner(System.in);
		
        try {
    		System.out.println("Enter Consumer Number: ");
    		consumerNo = sc2.nextInt();
    		if(consumerNo<0) {
    			throw new InputDataException("Invalid consumer number");
    		}
    		}
    		catch(InputDataException ide) {
    			System.out.print(ide);
    			System.exit(0);

    		}
    		catch(Exception e) {
    			System.out.print(e);
    			System.out.println(" : Invalid consumer number");
    			System.exit(0);
    		}
    		try {
    		System.out.println("Enter Consumer Name: ");
    		consumerName = sc2.next();
    		for(char c : consumerName.toCharArray()) {
    			if(!Character.isAlphabetic(c)) {
    				throw new InputDataException("invalid consumer name");
    			}
    		}
    		if((consumerName) == null) {
    			throw new InputDataException("invalid consumer name");
    		}
    		}catch(InputDataException ide) {
    			System.out.print(ide);
    			System.exit(0);


    		}catch(Exception e) {
    			System.out.print(e);
    			System.out.println(" : Invalid consumer number");
    			System.exit(0);
    		}
    		
    		
    		try {
    		System.out.println("Enter Current units: ");
    		currReading = sc2.nextInt(); 
    		if(currReading<prevReading) {
    			throw new InputDataException("Invalid Current reading");
    		}
    		}
    		catch(InputDataException ide) {
    			System.out.print(ide);
    			System.exit(0);

    		}
    		catch(Exception e) {
    			System.out.print(e);
    			System.out.println(" : Invalid consumer number");
    			System.exit(0);
    		}
    		int flag = 0;
    		for(Customer ec : dom_comm) {
    			if(ec.getConsumerNo() == consumerNo && ec.getConsumerName().equals(consumerName)) {
    				 if(ec.getEBConn() == 2) {
	    				ec.bill_history.add(new Bill(ec.getPrevReading(), ec.getCurrReading(), calculate_bill()));
	    				ec.setCurrReading(currReading);
	    				prevReading = ec.getPrevReading();
	    				ec.setUnits(currReading - ec.getPrevReading());
	    				ec.setBill(calculate_bill());
	    				flag = 1;
	    				break;
    				 }
    				 

    			}

    		}
    		
    		
    		if(flag == 0) {
    			Commercial ec = new Commercial();
    			ec.setConsumerNo(consumerNo);
    			ec.setConsumerName(consumerName);
    			ec.setEBConn(2);
    			ec.setCurrReading(currReading);
    			ec.setUnits(currReading - getPrevReading());
    			ec.setBill(calculate_bill());
    			dom_comm.add(ec);
    			ec.bill_history.add(new Bill( prevReading, currReading, calculate_bill()));
    		}
    	
	}

	@Override
	public double calculate_bill() throws Exception  {
		int units;
		double bill;
		units = currReading-prevReading;
		if(units >= 0 && units <= 100)
			bill = units * 2;
        else if( units>100 && units<= 200)
        	bill = (100*2) + ((units -100)*4.50);
        else if( units>200 && units<= 500)
        	bill = (100*2) + (100*4.50) + ((units - 200)*6);
        else 
        	bill = (100*2) + (100*4.50) + (300*6) + ((units-500)*7);

		if(units==0||bill<0) {
			throw new CalculationException("Invalid unit");
		}
		
		return bill;
		
		
	}

	@Override
	public void display()throws Exception {
	for(Customer c : dom_comm) {
		if(consumerNo == c.getConsumerNo()) {
			System.out.println("_________________________");
			System.out.println("ELECTRICITY BILL");
			System.out.println("__________________________");
			System.out.println("Consumer Number:" + c.getConsumerNo());
			System.out.println("Consumer Name:" + c.getConsumerName());
			System.out.println("Consumer Previous Reading:" + c.getPrevReading());
			System.out.println("Consumer Current Reading:" + c.getCurrReading());
			System.out.println("Type of Connection: Commercial" );
			System.out.println("________________________");
			System.out.println("Total Bill in Rs." + calculate_bill());
				
			}
		}
				
	}
	public String getCustomerDetails() {
		String result = "No record";
		for(Customer c : dom_comm) {
			
			if(consumerNo == c.getConsumerNo()) {
				result =  c.toString();
				System.out.println(c.getPrevReading());
				c.setPrevReading(c.getCurrReading());

			}
		}
		return result;
	}
		
}

/** 
 * @custom Exception is used to customize the exception according to the user need.
 * @InputDataException,@CalculationException,@ConnectionTypeException  are our custom exception.
 * 
 */

class InputDataException extends Exception{

	public InputDataException(String message) {
		super(message);
	}
	
}
class CalculationException extends Exception{

	public CalculationException(String message) {
		super(message);
	
		
	}
}
class ConnectionTypeException extends Exception{
	public ConnectionTypeException(String message) {
		super(message);
		
	}
}

/**
 *@DomesticBill is a extended class of Domestic which is used to handle the Domestic Bill related information.
 */

class Bill extends Domestic{
	int PrevReading;
	int CurrReading;
	double final_bill;
	
	public Bill(int PrevReading, int CurrReading, double final_bill) {
		this.PrevReading = PrevReading;
		this.CurrReading = CurrReading;
		this.final_bill = final_bill;
	}
	
	public void printBill() {
		System.out.println("Previous Reading = " + this.PrevReading + "\nCurrent Reading = " + this.CurrReading + "\nFinal bill = " + this.final_bill);
	}
}

