/** 
 * @custom Exception is used to customize the exception according to the user need.
 * @InputDataException,@CalculationException,@ConnectionTypeException  are our custom exception.
 * 
 */
class InputDataException extends Exception{

	public InputDataException(String message) {
		super(message);
	}
	
}
class CalculationException extends Exception{

	public CalculationException(String message) {
		super(message);
	
		
	}
}
class ConnectionTypeException extends Exception{
	public ConnectionTypeException(String message) {
		super(message);
		
	}
}