/**
 *@DomesticBill is a extended class of Domestic which is used to handle the Domestic Bill related information.
 */

class DomesticBill extends Domestic{
	int PrevReading;
	int CurrReading;
	double final_bill;
	
	public DomesticBill(int PrevReading, int CurrReading, double final_bill) {
		this.PrevReading = PrevReading;
		this.CurrReading = CurrReading;
		this.final_bill = final_bill;
	}
	
	public void printBill() {
		System.out.println("Previous Reading = " + this.PrevReading + "\nCurrent Reading = " + this.CurrReading + "\nFinal bill = " + this.final_bill);
	}
}
/**
 * @CommercialBill is a extended class of Domestic which is used to handle the Commercial Bill related information.
 */
class CommercialBill extends Commercial{
	int PrevReading;
	int CurrReading;
	double final_bill;

	public CommercialBill(int PrevReading, int CurrReading, double final_bill) {
		this.PrevReading = PrevReading;
		this.CurrReading = CurrReading;
		this.final_bill = final_bill;
	}
	
	public void printBill() {
		System.out.println("Previous Reading = " + this.PrevReading + "\nCurrent Reading = " + this.CurrReading + "\nFinal bill = " + this.final_bill);
	}
}