import java.util.ArrayList;

abstract class Customer{
	private int ConsumerNo;
	private String ConsumerName;
	private int  PrevReading = 0;
	private int CurrReading;
	private int EBConn;
	private int units;
	private double bill;
	ArrayList<DomesticBill> bill_history = new ArrayList<>();

	
	static ArrayList<Customer> dom_custs = new ArrayList<>();

	abstract public void input_data()throws Exception;
	abstract public double calculate_bill()throws Exception;
	abstract public void display()throws Exception;
	
	public int getConsumerNo() {
		return ConsumerNo;
	}
	public void setConsumerNo(int consumerNo) {
		ConsumerNo = consumerNo;
	}
	public String getConsumerName() {
		return ConsumerName;
	}
	public void setConsumerName(String consumerName) {
		ConsumerName = consumerName;
	}
	public int getPrevReading() {
		return PrevReading;
	}
	public void setPrevReading(int prevReading) {
		PrevReading = prevReading;
	}
	public int getCurrReading() {
		return CurrReading;
	}
	public void setCurrReading(int currReading) {
		CurrReading = currReading;
	}
	public int getEBConn() {
		return EBConn;
	}
	public void setEBConn(int eBConn) {
		EBConn = eBConn;
	}
	public int getUnits() {
		return units;
	}
	public void setUnits(int units) {
		this.units = units;
	}
	public double getBill() {
		return bill;
	}
	public void setBill(double bill) {
		this.bill = bill;
	}
	@Override
	public String toString() {
		return "Customer [ConsumerNo=" + this.ConsumerNo + ", ConsumerName=" + this.ConsumerName + ", PrevReading=" + this.PrevReading
				+ ", CurrReading=" + this.CurrReading + ", EBConn=" + this.EBConn + ", units=" + this.units + ", bill=" + this.bill + "]";
	}
}
